"""
La Programación y su Didáctica 2 en Exactas-UBA
Evaluación final - Trabajo con problemas en el aula

- biblioteca 'clasesAnteriores', contiene los ejericios previos 
"""
from  random import randint as azar

__all__=list()

# preguntar_frases_dos_aspectos: como comenzamos las preguntas que correspondan a cada aspecto
#
__all__.append("preguntar_frases_dos_aspectos")
preguntar_frases_dos_aspectos={"Estructural": ("tiene", "está compuesto por", "consiste en"),
                               "Funcional": ("sirve para","se usa para","funciona como para")}



__all__.append("concatenar_dos_listas")
def concatenar_dos_listas(lista1:list, lista2: list):
    """
    devuelve una lista con los elementos de las dos listas de entrada


    :param lista1:
    :param lista2:
    :return:
    """
    result = False
    #
    # Codígo de lxs estudiantes
    #
    result = lista1+lista2 #implementación de referencia
    return result

__all__.append("elemento_en_lista")
def elemento_en_lista(elemento, lista: list):
    """
    devuelve un booleano dependiendo si el elemento de entrada se encuentra o no en la lista de entrada

    :param elemento: elemento a buscar
    :param lista:
    :return: bool: está o no
    """
    result = False
    #
    # Codígo de lxs estudiantes
    #
    result = elemento in lista #implementación de referencia
    return result



__all__.append("elemento_aleatorio_de_lista")
def elemento_aleatorio_de_lista(lista: list):
    """
    devuelve un elemento al azar de la lista de entrada

    :param lista:
    :return: elemento al azar de la lista de entrada
    """
    result = False
    #
    # Codígo de lxs estudiantes
    #
    result = lista[azar(1, len(lista)) - 1] #implementación de referencia
    return result


