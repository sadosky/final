import random
from unittest import TestCase
import clasesAnteriores


class Test(TestCase):
    def test_concatenar_dos_listas(self):
        for lista1, lista2, esperado in (([0, 1], [2], [0, 1, 2]),
                                         ([], ['0', '1', 2], ['0', '1', 2])
                                         ):
            self.assertEqual(clasesAnteriores.concatenar_dos_listas(lista1, lista2),
                             esperado)  # lista1+lista2)

    def test_elemento_aleatorio_de_lista(self):
        nroGrande=1000
        lista=[0,1,2,3]
        mini = random.randint(4,nroGrande)
        maxi = random.randint(mini,mini+nroGrande)
        for _ in range(nroGrande):
            lista.append(random.randint(mini,maxi))
        while len(lista) >2:
            elem=clasesAnteriores.elemento_aleatorio_de_lista(lista)
            self.assertIn(elem,lista)
            lista.remove(elem)