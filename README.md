# La Programación y su Didáctica2 en Exactas-UBA 

## Trabajo Final

autor: Leandro Batlle <leandro.batlle@gmail.com>




### Informe y demo

- Ver el [informe adjunto]()
- código de la Demo/MVP en [éste repo](https://gitlab.com/sadosky/final) 
- Demo [en vivo](http://final.sadosky.batlle.ar) 


![Estructural vs Funcional](https://i.imgur.com/pz6T9HP.png)



## Objetivo


El objetivo de la presente clase (segunda clase de la serie) es integrar lo hecho durante la semana, para programar un **juego de preguntas/respuestas** donde la compu “adivina” nuestra selección de un objeto haciéndonos preguntas.

Los grupos contarán con éstas primitivas ya programadas en la paleta (ver autodoc en repo para más detalle):

- *siguiente_pregunta(respuesta):* motor para el juego de adivinazas. Descarta posibles cosas según respuesta recibida,y elije la proxima pregunta.
- *preguntar(caracteristica, aspecto):*  Qué preguntarle al usuarix, pero la entrada no es la oración entera: solo una característica posible de la cosa, y a qué aspecto correspondería.  El procedimiento completa la oración dependiendo del aspecto (estructural/funcional). Ver ejemplo en autodoc 


## Destino

Estudiantes de 1er de secundaria (~13 años), últimas semanas del año