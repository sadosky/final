"""
La Programación y su Didáctica 2 en Exactas-UBA
Evaluación final - Trabajo con problemas en el aula

- biblioteca 'clasesAnteriores', contiene los ejericios previos
"""

import csv,re,random,sys #stdlib
sys.path.append("..")
import clasesAnteriores

archivoCsvDefault = "Trabajo Final - Listas de ejemplo - Hoja 1.csv"
tabla_cosas = None
ultima_cosa = None #
ultimo_aspecto_estructural = None
ultima_caracteristica = None


cartel_de_arranque=f"""
{"---8<"*16}{chr(10)*3}
Este juego corresponde al trabajo final del curso La Programación y su Didáctica en Exactas2 - UBA - año 2021

Se trata de un juego de preguntas/respuestas donde la compu “adivina” nuestra selección de un objeto haciendonos preguntas.
Para mas info ver https://docs.google.com/document/d/1bRjwHDFVPJ0eLUJen3tluGIIxFwI8qLTH3MCDWM4Mvc

Las repuestas deben contener un si/no y un aspecto estructural/funcional. Ejemplos de respuestas válidas:
? si funcional
? si estructural
? no f
? N F
? si, FUNCIONAL
? S F 

Todas las respuestas deben ser consistentes con el objeto elegido al principio, si no, el programa se quedará sin preguntas y perderemos.
{chr(10)*3}{"---8<"*16}
"""

def cargar_cosas(archivoCsv=archivoCsvDefault):
    def partir_nombre_lista(nombre_lista: str):
        result=("N/A","N/A")
        try:
            nombre_lista=nombre_lista.lower().split("_")
            if len(nombre_lista) != 2:
                raise ValueError("no cumple el formato: 'cosa_aspecto' ")
            if nombre_lista[1][0] not in ("e","f"):
                raise ValueError(f"'{nombre_lista[1]}' no cumple con alguno de los dos aspectos: 'estructural' o 'funcional' ")
            aspecto = nombre_lista[1][0] == "e"
            #     aspecto="estructural"
            # else:
            #     aspecto="funcional"
        except Exception as e:
            #catch-all
            raise ValueError(f"""
la primera columna tiene q tener los nombres de los objetos, con el aspecto (estructural o funcional).
Elemplos:
bicicleta_estructuralmente
bicicleta_funcionalmente
moto_estructuralmente
moto_funcionalmente

error: {type(e)}:{e}
""")
        return nombre_lista[0],aspecto #result

    def partir_caracteristicas_lista(caracteristicas_lista: str, cosa: str, aspecto: str):
        result=list()
        try:
            caracteristicas_lista=caracteristicas_lista.split(",")
            if len(caracteristicas_lista) < 2:
                raise ValueError(f"la cosa '{cosa}', en su aspecto '{aspecto}', tiene menos de dos caracterisiticas {caracteristicas_lista} ")
            for caracteristica in caracteristicas_lista:
                #validamqnos? nah... "laburás, te cansás, que ganás?"
                result.append(caracteristica.replace('"',""))
        except Exception as e:
            # catch-all
            raise ValueError(f"""
la segunda columna tiene q tener una lista SEPARADA POR COMAS de caracteristiicas segun el aspecto corresponedietes,

ejemplo para el aspecto estructural de un gato: "cuatro patas", "una cola", "aparato ronroneador"
ejemplo para el aspecto funcional de un gato: "miau", "hacernos mimos", "cazar ratones"
 
luego se pconvertiran en preguntas como:
- Tu objeto tiene cuatro patas?
- Tu objeto sirve para hacer miau?

el formato de la segunda columna es de una LISTA de STRINGS: "caracteristica1","caracteristica2","caracteristica3".
Ojo las comillas y las comas

error: {type(e)}:{e}
""")
        return result


    result = dict()
    with open(archivoCsv, mode='r') as csv_file:
        try:
            csv_reader = csv.DictReader(csv_file)
            line_count = 0
            for row in csv_reader:
                #print(row)
                nombre,aspecto = partir_nombre_lista(row["Nombre_Lista"])
                if nombre not in result.keys():
                    result[nombre]=dict()
                result[nombre][aspecto] = partir_caracteristicas_lista(
                                                caracteristicas_lista=row["Caracteristicas_Lista"],
                                                cosa=nombre,aspecto=aspecto)
        except ValueError as e:
            raise ValueError(f"""problema dentro archivo '{archivoCsv}' con las caracteristicas:
            {type(e)}:{e}""")
        except KeyError as e:
            raise ValueError(f"""problema dentro archivo '{archivoCsv}' con las caracteristicas:
la PRIMERA fila tiene que tener éstos dos NOMBRES DE COLUMNA Nombre_Lista y Caracteristicas_Lista """)
        except Exception as e:
            print(f"""
            
            =====================================================
            problema cargando archivo CSV con las caracteristicas ({archivoCsv})
            =====================================================
            
            """)
            raise
    return result

respuesta_valida_re =  re.compile("^(?P<afirmativa>[NS][A-Z0-9_]*)(?P<separador>[_, \W]+)(?P<estructural>[EF].*)") # cualquier cosa -> https://regex101.com/r/WE5IEE/1
def respuesta_valida(respuesta):
    """
    valida que la respuesta conteste dos cosas:
     - si o no (s... S..., n..., etc)
     - aspecto (estructural, funcional, e*, f*, E*, F*)

     separados por un espacio, parentesis o coma (cualquier signo de puntuación)

    :param respuesta: str: formato si_estructural, no_estructural,si_funcional, no_funcional, 
    :return: None para invalida, o tupla por (afirmativo, estructural)
    """
    result = None
    try:
        result=respuesta_valida_re.fullmatch(respuesta.upper())
        if result is not None:
            #viene codigo compacto y feo, feo... pero condice con respuesta_valida_re
            result=(result.groupdict()["afirmativa"][0]=="S",
                    result.groupdict()["estructural"][0] == "E")
    except Exception as e:
        pass
    return result


def siguiente_pregunta(respuesta="inicializar",archivoCsv=archivoCsvDefault):
    """
    motor para el juego de adivinazas. Descarta posibles cosas según respuesta recibida,y elije la proxima pregunta.

    Cada corrida devuelve una lista con dos elementos:
        lista[0]: la siguiente pregunta (si el juego sigue, también puede ser None si el juego se terminó)
        lista[1]: una lista con las posibles cosas a adivinar

    sintaxis de uso
    ===============

    pregunta, cosas_posibles = siguiente_pregunta("si, funcional")     #sintaxis de uso



    Siguiente pregunta
    ==================

    Situaciones en las que la siguiente pregunta es None y el juego se terminó:

    None) cuando solo queda una cosa posible. GANAMOS!!!!
    None) cuando la respuesta es mal formada. PERDIMOS.... Ver autodoc de la función respuesta_valida()
    None) cuando la respuesta confiene un aspecto incorrecto. PERDIMOS... Ejemplo: debería ser "Estructural" y contestamos "Funcional"

    Situaciones en las que la siguiente pregunta es un STRING y el juego sigue:
    String) cuando la respuesta confiene el aspecto correcto. Ejemplo: debería ser "Funcional" y contestamos "Funcional"
    String) responde si o no, OJO: una mala respuesta que puede llevar a perder...


    :param respuesta: str: si/no y aspecto (si es None carga el arhuivo de cosas). Ver autodoc de respuesta_valida()
    :param archivoCsv: str: opcional, PARA PROFES: archivo a CSV cargar para inicializar lista de cosas
    :return: tupla:
            str: caraccteritica de la proxima pregunta (o None, ver arriba)
            str: aspecto de la proxima pregunta (o None, ver arriba)
            list(): cosas_posibles
    """
    global ultimo_aspecto_estructural #uso globales para minimizar el encabezado
    global ultimo_afirmativa
    global ultima_cosa
    global ultima_caracteristica
    global tabla_cosas
    result_pregunta = "N/A"
    inicializar = (tabla_cosas is None) or  (respuesta == "inicializar")

    if inicializar:
        tabla_cosas = cargar_cosas(archivoCsv)
        ultimo_aspecto_estructural = random.random() > 0.5
        afirmativa,aspecto = None,ultimo_aspecto_estructural
    else:
        if respuesta_valida(respuesta) is None:
            #caso None) cuando la respuesta confiene un aspecto incorrecto. PERDIMOS...
            result_pregunta = None
            afirmativa,aspecto=None,None
        else:
            afirmativa,aspecto=respuesta_valida(respuesta)

            if aspecto != ultimo_aspecto_estructural:
                #caso None) cuando la respuesta confiene un aspecto incorrecto. PERDIMOS...
                result_pregunta = None
                aspecto = None
            else:
                # paso todas las pruebas... ahora a descartar cosas según argumento de afirmativa
                cosas_a_descartar=list()
                for cosaKey in tabla_cosas.keys():
                    cosa = tabla_cosas[cosaKey]
                    if (ultima_caracteristica in cosa[aspecto]) != afirmativa:
                        #René Lavan diría "no se puede hacer menos compacto..." :)
                        cosas_a_descartar.append(cosaKey)
                msg="cosas descartadas: "
                for cosaKey in cosas_a_descartar:
                    cosa=tabla_cosas.pop(cosaKey)
                    msg+=f"{cosaKey}, "
                print(msg)

                #
                #  ejercicio para la vuelta de las vacas:
                #   insertar acá código para que no vuelva a repetir preguntas
                #
                #
                aspecto=not(aspecto) #el enunciado pide alternar estrivtamente

    if len(tabla_cosas.keys())<2 or respuesta == "fin":
        #caso None) cuando solo queda una cosa posible. GANAMOS!!!!
        #caso None) no quedan cosas posibles. PERDIMOS... seguro respondimos mal alguna de las anteriores
        result_pregunta = None
    else:
        if result_pregunta == "N/A" or inicializar:
            # paso todas las pruebas... ahora a elegir la proxima pregunta
            tabla_cosas_keys=list(tabla_cosas.keys())
            indice = random.randint(1, len(tabla_cosas_keys)) - 1
            cosa=tabla_cosas[tabla_cosas_keys[indice]]
            caracteristicas_un_aspecto=cosa[aspecto]
            indice = random.randint(1, len(caracteristicas_un_aspecto)) - 1
            ultima_caracteristica = caracteristicas_un_aspecto[indice]
            ultimo_aspecto_estructural = aspecto
            result_pregunta = caracteristicas_un_aspecto[indice]

    return result_pregunta,aspecto,list(tabla_cosas.keys())


def preguntar(caracteristica: str, aspecto: bool, cosas_posibles=list()):
    """
    Qué preguntarle al usuarix, pero la entradano es la oración entera: solo una característica posible de la cosa, y a qué aspecto correspondería.
    El procedimiento completa la oración dependiendo de el aspecto. Ejemplo

    preguntar("colgarle ropa a secar",Funcional)

    puede tener como salida "nuestra cosa se usa para  colgarle ropa a secar ??"

    :param caracteristica: str: cualquier caracteristica de la cosa
    :param aspecto: bool: True para "Estructural", caso contrario "Funcional"
    :param cosas_posibles: list: opcionalmente, muestra al usuario las posibles cosas
    :return:
    """

    if aspecto:
        aspecto = "Estructural"
    else:
        aspecto = "Funcional"
    preguntar_frases_un_aspecto=clasesAnteriores.preguntar_frases_dos_aspectos[aspecto]
    indice= random.randint(1, len(preguntar_frases_un_aspecto)) - 1
    preguntar_frases=preguntar_frases_un_aspecto[indice]
    pregunta=f"""---\nnuestra cosa {preguntar_frases} {caracteristica} ??"""
    if len(cosas_posibles) > 0:
        print(f"restan por descartar: {', '.join(cosas_posibles)}")
    result=input(pregunta)
    return result


def decir(msg: str):
    print(msg)
    return None