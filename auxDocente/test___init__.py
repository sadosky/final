from unittest import TestCase
import sys

sys.path.append("..")
from auxDocente import (cargar_cosas,
                        siguiente_pregunta, respuesta_valida, preguntar)

# pregunta_test_cases = bool: estructural/¬funcional, str:
pregunta_test_cases = [(True, "algo estructural"), #estructural
                        (True, "algo estructural"),
                        (False, "algo funcional"),
                        (False, "algo funcional")]


# respuesta_test_cases = str: respuesta, bool: valido, bool: si/no, bool: estructural/¬funcional
respuesta_test_cases = [["Si(Estructural)", (True, True)],
                        ["No, Funcional", (False, False)],
                        ["SIXXXXXX_Funcional", (True, False)],
                        ["sI Funcional", (True, False)],
                        ["n Estructural", (False, True)],
                        ["n f", (False, False)],
                        ["SiEstructural)", None],
                        ["No, uncional", None],
                        ["XXXXXX_Funcional", None],
                        ["Si(structural)", None],
                        ["Nouncional", None],
                        ["Si", None],
                        ["No", None],
                        [" Estructural", None],
                        ["Estructural", None],
                        ["", None],
                        ["", None],
                        ["", None],
                        ["", None],
                        ["", None]]


class Test(TestCase):
    def test_cargar_cosas(self):
        # self.fail()
        tabla_cosas = cargar_cosas(archivoCsv="test.csv")
        print("ok")

    def test_cargar_cosas_fail1(self):
        try:
            tabla_cosas = cargar_cosas(archivoCsv="test_fail1.csv")
        except Exception as e:
            print(f"ok: {type(e)}:{e}")
        else:
            self.fail("este test deberia fallar...")

    def test_cargar_cosas_fail2(self):
        try:
            tabla_cosas = cargar_cosas(archivoCsv="test_fail2.csv")
        except Exception as e:
            print(f"ok: {type(e)}:{e}")
        else:
            self.fail("este test deberia fallar...")

    def test_siguiente_pregunta_inicial(self):
        # tabla_cosas = None
        respuesta = None
        cosas_posibles = None
        pregunta, aspecto, cosas_posibles = siguiente_pregunta(respuesta, archivoCsv="test.csv")

        print("")

    def test_respuesta_valida_formato(self):
        for entrada, salida_esperada in respuesta_test_cases:
            if salida_esperada is None:
                self.assertIsNone(respuesta_valida(entrada),
                                  f"respuesta_valida('{entrada}') -> {respuesta_valida(entrada)}: validación debería devolver None")
            else:
                self.assertIsInstance(respuesta_valida(entrada), tuple,
                                      f"respuesta_valida('{entrada}') -> {respuesta_valida(entrada)}: formato válido, debería devolver tupla")
        print("respuesta_valida(): procesa OK el formato compuesto")

    def test_respuesta_valida_afirmativa(self):
        # for entrada,_respuesta_valida,salida_esperada,_aspecto_estructural in respuesta_test_cases:
        for entrada, salida_esperada in respuesta_test_cases:
            if salida_esperada is not None:
                self.assertEqual(salida_esperada[0], respuesta_valida(entrada)[0],
                                 f"respuesta_valida('{entrada}') -> {respuesta_valida(entrada)}: falla en pocesar primera parte si/no")
        print("respuesta_valida(): procesa OK la afirmativa")

    def test_respuesta_valida_estructural(self):
        # for entrada,_respuesta_valida,salida_esperada,_aspecto_estructural in respuesta_test_cases:

        for entrada, salida_esperada in respuesta_test_cases:
            if salida_esperada is not None:
                self.assertEqual(salida_esperada[1], respuesta_valida(entrada)[1],
                                 f"respuesta_afirmativa({entrada}) -> {respuesta_valida(entrada)}: falla en prcesar segunda parte estructural/¬funcional")
        print("respuesta_valida(): procesa OK el aspecto")


    def test_preguntar(self):
        print("prueba de preguntar(), enter o para terminar OK tipear OK, para terminar tipear algo")
        for caracteristica, aspecto in pregunta_test_cases:
            respuesta=preguntar(caracteristica, aspecto, ("c1","c2"))
            if respuesta == "OK":
                break
            elif respuesta != "":
                self.fail()
