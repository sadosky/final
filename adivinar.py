"""
La Programación y su Didáctica 2 en Exactas-UBA
Evaluación final - Trabajo con problemas en el aula

Este es el programa que deben construir lxs estudioadne durante la clase en cuestión.  ver informe.

Precondicones: ver informe. Antes implementaron los procedimientos de la bilioteca "clasesAnteriores"
Poscondiciones:  ver informe. con el programa realizado,  y luego de jugar varias veces, practican busquedas en buscadores de imágenes (pexels.com, etc)

"""

from auxDocente import (siguiente_pregunta,
                        preguntar, decir, cartel_de_arranque)

from clasesAnteriores import *

def resolución_ejercicio():
    respuesta = "inicializar"
    #comienza un nuevo juego
    caracteristica, aspecto, cosas_posibles = siguiente_pregunta()  # si respuesta == None inicializa
    respuesta = None
    decir(cartel_de_arranque)
    decir(f"Comenzemos! dale que tu objeto es {elemento_aleatorio_de_lista(cosas_posibles).upper()}?? (si me desconfiás, elejí cualquiera de la lista)")
    while (caracteristica is not None):
        respuesta = preguntar(caracteristica, aspecto, cosas_posibles)
        caracteristica, aspecto, cosas_posibles = siguiente_pregunta(respuesta)
        if len(cosas_posibles) == 1:
            decir(f"GANAMOS!! tu objeto es {cosas_posibles[0].upper()}  :)")
        elif len(cosas_posibles) == 0:
            decir(f" Perdimos: las respuestas que diste no corresponden a ninguna cosa conocida :,(")
        elif aspecto is None:
            decir(f""" Perdimos: confundiste el aspecto estructural con el funcional de un sistema :,( 
            (no podremos seguir descartando opciones)""")
        else:
            #decir("ERROR!!! pasó algo que mi programa no entiende :,(")
            #decir(f"Los objetos que quedaron en el tintero son {cosas_posibles}")
            pass



if __name__ == '__main__':
    resolución_ejercicio()


